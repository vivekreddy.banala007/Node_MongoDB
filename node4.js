var MongoClient=require('mongodb').MongoClient,express=require('express'),app=express(),bodyParser=require('body-parser');
app.use(express.static('public'));
const mongodb_conn='mongodb://localhost:27017';
var db='';
var obj;
var id;
app.get('/',function(req,res){
    res.sendFile(__dirname+'/mongodb.html');
});
app.get('/getusers',function(req,res){
    db.collection('users').find().toArray(function(err,doc){
        res.end(JSON.stringify(doc));
    });
});
app.post('/getres/:id',function(req,res){
   obj=JSON.parse(req.params.id);
    
    try{
        db.collection('users').insert(obj);
    }
    catch(e){
        console.log("error");
    }
    res.end("Table updated");
});
app.post('/delres/:id',function(req,res){
    id={
        "user_id":Number(req.params.id)
    };
     try{
         db.collection('users').remove(id);
     }
     catch(e){
         console.log("error");
     }
     res.end("Table deleted");
 });
MongoClient.connect(mongodb_conn,function(err,client){
    if(!err){
        console.log("Connected successfully");
        app.listen(3000,function(){
            db=client.db('test');
        });
        
        
    }
});